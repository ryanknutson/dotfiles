# ryanknut's dotfiles

## Install vim-plug

run this in a terminal
```
curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
```
