call plug#begin()
" The default plugin directory will be as follows:
"   - Vim (Linux/macOS): '~/.vim/plugged'
"   - Vim (Windows): '~/vimfiles/plugged'
"   - Neovim (Linux/macOS/Windows): stdpath('data') . '/plugged'
" You can specify a custom plugin directory by passing it as the argument
"   - e.g. `call plug#begin('~/.vim/plugged')`
"   - Avoid using standard Vim directory names like 'plugin'

" Make sure you use single quotes

" easily comment things out
Plug 'tpope/vim-commentary'

" vim surround, rlly useful with quotes, braces, etc.
Plug 'tpope/vim-surround'

" file browser
Plug 'scrooloose/nerdtree'

" Git plugin
Plug 'tpope/vim-fugitive'

" syntax checking
" !!! only works on Vim 8 and NeoVim !!!
Plug 'dense-analysis/ale'

" Pug plugin
Plug 'digitaltoad/vim-pug'

" Initialize plugin system
" - Automatically executes `filetype plugin indent on` and `syntax enable`.
call plug#end()

" type Nonu to hide line numbering, Nu will re-enable
command Nonu :set nornu nu!
command Nu :set rnu nu

map <F5> :NERDTreeToggle<CR>

" this is not needed if using vim-plug (above)
" syntax enable
" filetype plugin indent on

" colorscheme (well duh)
" elflord, desert, ron, and murphy
" all work well with current setup
colorscheme elflord

" tab to spaces
set expandtab
" set tab width to 2 columns
set tabstop=2
" use 2 columns for indention
set shiftwidth=2

" always show status bar
" set laststatus=2
" set statusline=%f "tail of the filename

" start search on input, and highlight
set incsearch
set hlsearch

" line numbering
" having both of these enabled gives
" you fancy hybrid line numbering
set number
set relativenumber
highlight LineNr ctermfg=cyan

" code folding
" set foldenable
" set foldlevelstart=10
" set foldmethod=syntax
" use space to control folds
" noremap space za

" disable bold (I don't like the look)
set t_md=

" show where brackets start/end
set showmatch
