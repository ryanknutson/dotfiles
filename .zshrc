# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
setopt beep
bindkey -v
# End of lines configured by zsh-newuser-install

# zpm (zsh package manager)
if [[ ! -f ~/.zpm/zpm.zsh ]]; then
    git clone --recursive https://github.com/zpm-zsh/zpm ~/.zpm
fi
source ~/.zpm/zpm.zsh

# history
setopt HIST_SAVE_NO_DUPS
setopt HIST_EXPIRE_DUPS_FIRST
setopt SHARE_HISTORY

# autocomplete based on history
bindkey '\e[A' history-search-backward
bindkey '\e[B' history-search-forward


# aliases

## yt-dlp
## you can change to `youtube-dl` if you use that
alias ytmp4="yt-dlp --recode-video mp4"
alias ytmp3="yt-dlp -x --audio-format mp3"
alias ytinfo="yt-dlp --write-description --write-info-json --write-annotations --write-sub --write-thumbnail"
alias ytf="yt-dlp --list-formats"

# vimcat
# remove line numbers
alias vimcat="vimcat --cmd=\"set nonumber\" --cmd=\"set norelativenumber\""

alias lsa="ls -a"

## show only hidden files in ls
alias lshdn="ls -A | grep "^\.""

## alias for ls to print chmod numbers
alias cls="ls -l | awk '{k=0;for(i=0;i<=8;i++)k+=((substr(\$1,i+2,1)~/[rwx]/)*2^(8-i));if(k)printf(\"%0o \",k);print}'"

# platform specific tools
case "$OSTYPE" in
  darwin*)
    # macOS
    uname -a
    autoload -U colors && colors
    export CLICOLOR=1
    alias l='ls -G -l'
    # fix for pandoc not finding LaTeX on certain versions of macOS
    alias pandoc='pandoc --pdf-engine=/Library/TeX/texbin/pdflatex'
  ;;
  linux*)
		autoload -U colors && colors
		export CLICOLOR=1
		export LSCOLORS=ExGxBxDxCxEgEdxbxgxcxd
		alias ls='ls --color=auto'
		alias l='ls --color=auto -lh'
  ;;
esac


# prompt colors
if [[ -f "$HOME/.zshpromptcolors" ]]; then
  source ~/.zshpromptcolors
else
  COLOR1=cyan
  COLOR2=white
  COLOR3=white
  COLOR4=cyan
  COLOR5=white
fi

# colors look weird on Linux console, change them
# (aka tty/boot up console)
# this version here set the console to 16 color mode
if [[ "$TERM" = "linux" ]]; then
  COLOR1=cyan
  COLOR2=white
  COLOR3=magenta
  COLOR4=yellow
fi

# prompt
# PS1="%F{$COLOR1}%n%f%F{$COLOR2}@%f%F{$COLOR3}%m%f %F{$COLOR4}%~%f %% "
PS1="%F{$COLOR1}%n%f%F{$COLOR2}@%f%F{$COLOR3}%m%f %F{$COLOR4}%~%f %F{$COLOR5}%%%f "

# prompt for root
# PS1="%f%F{$COLOR3}%m%f %F{$COLOR4}%~%f %F{$COLOR5}%#%f "

# The following lines were added by compinstall
# zstyle :compinstall filename '/Users/ryanknut/.zshrc'

# autocomplete
autoload -Uz compinit
compinit

# case-insensitive tab completion
# pick one below

# match when using lowercase, ignore when uppercase
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Za-z}'
# match lowercase & uppercase
# zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}'
# only match when no case sensitive matches
# zstyle ':completion:*' matcher-list '' 'm:{a-zA-Z}={A-Za-z}'

# End of lines added by compinstall


# zsh git aliases from oh-my-zsh
if [[ -f "$HOME/.zshalias/git" ]]; then
  source ~/.zshalias/git
fi

export PAGER=less
export VISUAL=vim
export EDITOR=vim
export PATH="/usr/local/sbin:/usr/local/bin:$PATH"
